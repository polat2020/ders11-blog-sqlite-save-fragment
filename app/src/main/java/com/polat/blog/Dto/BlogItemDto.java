package com.polat.blog.Dto;

public class BlogItemDto {
    int id;
    String name;
    String detail;
    byte[] image;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {return detail;}

    public void setDetail(String detail) {this.detail = detail;}

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) { this.image = image;}
}
