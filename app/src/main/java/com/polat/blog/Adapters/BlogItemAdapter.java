package com.polat.blog.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.polat.blog.Dto.BlogItemDto;
import com.polat.blog.Holders.BlogItemHolder;
import com.polat.blog.R;
import com.polat.blog.databinding.RecyclerRowBinding;

import java.util.ArrayList;

public class BlogItemAdapter extends RecyclerView.Adapter<BlogItemHolder> {

    ArrayList<BlogItemDto> blogItemArrayList;
    public BlogItemAdapter(ArrayList<BlogItemDto> blogItemArrayList) {
        this.blogItemArrayList = blogItemArrayList;
    }

    @Override
    public BlogItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        RecyclerRowBinding binding =
                RecyclerRowBinding.inflate(LayoutInflater.from(context), parent, false);
        return new BlogItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BlogItemHolder holder, int position) {
        BlogItemDto item = blogItemArrayList.get(position);
        holder.binding.nameText.setText(item.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt("id", item.getId());
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_detailViewFragment, bundle);

            }
        });
    }

    @Override
    public int getItemCount() {
        return blogItemArrayList.size();
    }

}
