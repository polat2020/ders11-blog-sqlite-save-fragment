package com.polat.blog.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.polat.blog.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.menu_blog_ekle) {

            //Yöntem 1
            NavController controller= Navigation.findNavController(this,R.id.fragmentContainerView);
            controller.navigate(R.id.action_mainFragment_to_detailFragment);

            //Yöntem 2
//            NavDirections action=  MainFragmentDirections.actionMainFragmentToDetailFragment();
//            Navigation.findNavController(this,R.id.fragmentContainerView).navigate(action);
        }

        return super.onOptionsItemSelected(item);
    }
}