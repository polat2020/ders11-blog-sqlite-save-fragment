package com.polat.blog.Holders;

import androidx.recyclerview.widget.RecyclerView;

import com.polat.blog.databinding.RecyclerRowBinding;

public class BlogItemHolder extends RecyclerView.ViewHolder {
    public RecyclerRowBinding binding;

    public BlogItemHolder(RecyclerRowBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
