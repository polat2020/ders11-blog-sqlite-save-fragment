package com.polat.blog.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polat.blog.Adapters.BlogItemAdapter;
import com.polat.blog.Db.DatabaseHelper;
import com.polat.blog.Dto.BlogItemDto;
import com.polat.blog.R;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    BlogItemAdapter blogItemAdapter;
    DatabaseHelper db;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        db = new DatabaseHelper(getContext());
        getData(view);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        //Inflater
//
//        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.blog_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void getData(View view) {
        try {
            ArrayList<BlogItemDto> list = db.getirList();
            blogItemAdapter = new BlogItemAdapter(list);
            RecyclerView recyclerView= view.findViewById(R.id.recyclerView) ;
            recyclerView.setAdapter(blogItemAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}