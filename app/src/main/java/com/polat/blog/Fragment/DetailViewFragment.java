package com.polat.blog.Fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.polat.blog.Db.DatabaseHelper;
import com.polat.blog.Dto.BlogItemDto;
import com.polat.blog.R;


public class DetailViewFragment extends Fragment {

    DatabaseHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_view, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        if (getArguments() == null) {
            Log.e("Hata", "onViewCreated: arguments hatası");
            return;
        }

        Log.d("Id", "onViewCreated: " + getArguments().getInt("id"));
        db = new DatabaseHelper(getContext());

        Integer id = getArguments().getInt("id", 0);

        try {

            BlogItemDto item = db.getir(id);

            TextView blogNameText= view.findViewById(R.id.blogNameText);
            TextView detailText= view.findViewById(R.id.detailText);
            ImageView imageView= view.findViewById(R.id.imageView);

            blogNameText.setText(item.getName());
            detailText.setText(item.getDetail());
            Bitmap bitmap = BitmapFactory.decodeByteArray(item.getImage(), 0, item.getImage().length);
            imageView.setImageBitmap(bitmap);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}