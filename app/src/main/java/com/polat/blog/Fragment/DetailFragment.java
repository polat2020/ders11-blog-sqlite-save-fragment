package com.polat.blog.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.polat.blog.Db.DatabaseHelper;
import com.polat.blog.Helper.BitmapHelper;
import com.polat.blog.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DetailFragment extends Fragment {

    Bitmap selectedImage;
    ActivityResultLauncher<Intent> activityResultLauncher;
    ActivityResultLauncher<String> permissionLauncher;
    DatabaseHelper db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=  inflater.inflate(R.layout.fragment_detail, container, false);
       return  v;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        registerLauncher(view);
        TextView blogNameText= view.findViewById(R.id.blogNameText);
        TextView detailText= view.findViewById(R.id.detailText);
        ImageView imageView= view.findViewById(R.id.imageView);
        Button button= view.findViewById(R.id.button);

        db = new DatabaseHelper(view.getContext());
        blogNameText.setText("");
        detailText.setText("");

        Bitmap selectImage = BitmapFactory.decodeResource(view.getResources(),
                R.drawable.select);
        imageView.setImageBitmap(selectImage);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save(view);
            }
        });


        imageView.setOnClickListener(view1 -> {
            selectImage(view1);
        });
    }


    public void registerLauncher(View view) {
        ImageView imageView= view.findViewById(R.id.imageView);


        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent intentFromResult = result.getData();
                            if (intentFromResult != null) {
                                Uri imageData = intentFromResult.getData();
                                try {

                                    if (Build.VERSION.SDK_INT >= 28) {
                                        ImageDecoder.Source source = ImageDecoder.createSource(view.getContext().getContentResolver(), imageData);
                                        selectedImage = ImageDecoder.decodeBitmap(source);
                                        imageView.setImageBitmap(selectedImage);

                                    } else {
                                        selectedImage = MediaStore.Images.Media.getBitmap(view.getContext().getContentResolver(), imageData);
                                        imageView.setImageBitmap(selectedImage);
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }
                });


        permissionLauncher =
                registerForActivityResult(new ActivityResultContracts.RequestPermission(),
                        new ActivityResultCallback<Boolean>() {
                            @Override
                            public void onActivityResult(Boolean result) {
                                if (result) {
                                    //izin verildi
                                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    activityResultLauncher.launch(intent);

                                } else {
                                    //izin verilmedi
                                    Toast.makeText(view.getContext(), "İzin gerekiyor.!", Toast.LENGTH_LONG).show();
                                }
                            }

                        });
    }

    public void selectImage(View view) {

        if (ContextCompat.checkSelfPermission(view.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Snackbar.make(view, "Galeriye Erişim için izin istiyorum", Snackbar.LENGTH_INDEFINITE)
                        .setAction("İzin ver", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                permissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
                            }
                        }).show();
            } else {
                permissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        } else {
            Intent intentToGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            activityResultLauncher.launch(intentToGallery);
        }

    }

    public void save(View view) {
        TextView blogNameText= getView().findViewById(R.id.blogNameText);
        TextView detailText= getView().findViewById(R.id.detailText);

        String name = blogNameText.getText().toString();
        String detail = detailText.getText().toString();

        Bitmap smallImage = BitmapHelper.makeSmallerImage(selectedImage, 300);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        smallImage.compress(Bitmap.CompressFormat.PNG, 60, outputStream);
        byte[] byteArray = outputStream.toByteArray();

        try {

            db.ekle(name, detail, byteArray);


        } catch (Exception e) {

        }

        Navigation.findNavController(getView()).navigate(R.id.action_detailFragment_to_mainFragment);

    }
}