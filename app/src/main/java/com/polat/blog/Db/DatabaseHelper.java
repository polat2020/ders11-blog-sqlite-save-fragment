package com.polat.blog.Db;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.polat.blog.Dto.BlogItemDto;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper {
    private Context context;
    private SQLiteDatabase database;

    public DatabaseHelper(Context context) {
        this.context = context;
        database = this.context.openOrCreateDatabase("Blogs", MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS blogs " +
                "(id INTEGER PRIMARY KEY,name VARCHAR," +
                " detail VARCHAR,image BLOB)");
    }

    public void ekle(String name, String detail, byte[] byteArray) {
        String sqlString = "INSERT INTO blogs (name, detail, image) VALUES (?, ?, ?)";
        SQLiteStatement sqLiteStatement = database.compileStatement(sqlString);
        sqLiteStatement.bindString(1, name);
        sqLiteStatement.bindString(2, detail);
        sqLiteStatement.bindBlob(3, byteArray);
        sqLiteStatement.execute();
    }

    @SuppressLint("Range")
    public BlogItemDto getir(int id) {
        Cursor cursor = database.rawQuery("SELECT * FROM blogs WHERE id = ?", new String[]{String.valueOf(id)});
        BlogItemDto item = new BlogItemDto();
        while (cursor.moveToNext()) {
            item.setName(cursor.getString(cursor.getColumnIndex("name")));
            item.setDetail(cursor.getString(cursor.getColumnIndex("detail")));
            byte[] bytes = cursor.getBlob(cursor.getColumnIndex("image"));
            item.setImage(bytes);
        }
        cursor.close();
        return item;
    }

    @SuppressLint("Range")
    public ArrayList<BlogItemDto> getirList() {
        Cursor cursor = database.rawQuery("SELECT * FROM blogs", null);
        ArrayList<BlogItemDto> list = new ArrayList<BlogItemDto>();
        while (cursor.moveToNext()) {
            BlogItemDto item = new BlogItemDto();
            item.setId(cursor.getInt(cursor.getColumnIndex("id")));
            item.setName(cursor.getString(cursor.getColumnIndex("name")));
            item.setDetail(cursor.getString(cursor.getColumnIndex("detail")));
            byte[] bytes = cursor.getBlob(cursor.getColumnIndex("image"));
            item.setImage(bytes);
            list.add(item);
        }
        cursor.close();
        return list;
    }
}
